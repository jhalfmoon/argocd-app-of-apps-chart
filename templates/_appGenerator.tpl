{{- /* =====================================================*/}}
{{- define "appOfApps.mergeAndGenerateApps" }}
{{- /* =====================================================*/}}
{{- /*
In:
    * .Values.ConfigDirs ; List of diriectories to traverse containing values.
Does:
    * See the comments for the includeded templates for details.
Out:
    * ArgoCD application manifests for the values defined in the passed variable
*/}}
{{ include "appOfApps.mergeFolders" . }}
{{ include "appOfApps.generateApps" . }}
{{- end }}

{{- /* =====================================================*/}}
{{- define "appOfApps.mergeFolders" }}
{{- /* =====================================================*/}}
{{- /*
In:
    * .Values.ConfigDirs ; List of diriectories to traverse. One directory will suffice in most cases.
Does:
    * Traverse the list of directories
    * Reads all *.yaml files in each directory
    * Merges the values of all found yaml files into $mergedConfig
    * Inject $mergedConfig into root context as .Values.mergeConfig
Out:
    * .Values.mergedConfig contains the generated output
*/}}
{{- $mergedConfig := (dict) }}
{{- range $configDir := .Values.configDirs }}
  {{- with $ }}
  {{- range $path, $_ := .Files.Glob (print $configDir "/" "**.yaml") }}
    {{- with $ }}
    {{- $mergedConfig = mergeOverwrite (dict) $mergedConfig (.Files.Get $path | fromYaml) }}
    {{- end }}
  {{- end }}
  {{- end }}
{{- end }}
{{- /* Inject the generated variable into .Variables of the caller  */}}
{{- /* https://helm.sh/docs/chart_template_guide/function_list/#set */}}
{{- $_ := set $.Values "mergedConfig" $mergedConfig }}
{{- end }}

{{- /* =====================================================*/}}
{{- define "appOfApps.generateApps" }}
{{- /* =====================================================*/}}
{{- /*
In:
    * Context must be set to $.Values.mergedConfig, which contains appGroup and default values. See example value file for structure of values.
Does:
    * For each defined appGroup
        - merge the appGroup.defaults with default.appGroupDefaults
        - merge the appGroup.apps with default.apps
        - generate an application manifest for each app, based on app settings merged with default settings
Out:
    * ArgoCD application manifests for the values defined in the passed variable
*/}}
{{- /* Loop over each appGroup */}}
{{- range $groupName,$groupVal := .Values.mergedConfig.appGroups }}
#=== AppGroup: {{ $groupName }} ======================

{{- /* Merge the optional default apps into each appGroup */}}
{{- $apps := mergeOverwrite (deepCopy (default (dict) (($.Values.mergedConfig).default).apps)) (deepCopy ($groupVal).apps) }}

{{- /* Loop over each app in the appGroup */}}
{{- range $appName,$appVal := $apps }}

{{- /* An app must have at least one parameter (path,repoURL etc) excplictely defined ie. not from defaults */}}
{{- /* This is to prevent accidental empty apps with only "enabled" set and nothing more */}}
{{- if not $appVal.source }}
{{- fail (print "ERROR: At least one rA path and/or repoUrl must be defined for directory: " $.Values.configDirs ", group: " $groupName ", app: " $appName) }}
{{- end }}

{{- /* Merge default values with custom values - https://olof.tech/helm-values-inheritance-one-liner/ */}}
{{- $appConfig := mergeOverwrite (deepCopy (default (dict) (($.Values.mergedConfig).default).groupDefaults)) (deepCopy (default (dict) ($groupVal).groupDefaults)) $appVal}}

{{- if ($appConfig.enabled | required "ERROR: app.enabled not defined") }}
{{- /* This is used when isHelm is true - generates and set generated helm value file */}}
{{- $defaultHelmConfig := (print "helm:\n  valueFiles:\n    - values.yaml\n    - values-" $groupName ".yaml") }}

{{- /* Generate a default name to use to use later, when no specific name is given */}}
{{- $generatedName := (print $appName ) }}
{{- if eq $appConfig.groupNamePosition "prefix" }}
  {{- $generatedName = (print $groupName "-" $appName ) }}
{{- else if (eq $appConfig.groupNamePosition "suffix") }}
  {{- $generatedName = (print $appName "-" $groupName ) }}
{{- end -}}

{{- /* Append basepath if it is defined. $path defaults to '.' */}}
{{- $path := default "." $appConfig.source.path }}
{{- if $appConfig.source.basePath }}
  {{- $path = (print $appConfig.source.basePath "/" $path) }}
{{- end }}

#=== App: {{ $generatedName }} ======================
{{- /* Uncomment this bit to help debug this code
{{- $appConfig := mergeOverwrite (deepCopy $.default.groupDefaults) (deepCopy $groupVal.groupDefaults) (deepCopy $appVal) }}
#-----------------------
{{ toYaml $appConfig }}
#-----------------------
{{ toYaml $apps }}
#-----------------------
{{ toYaml $groupVal.apps }}
#-----------------------
{{ toYaml $.default.apps }}
*/}}
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: {{ $appConfig.name | default $generatedName }}
  namespace: {{ $appConfig.argoNamespace }}
  labels:
    argocdRootGroup: {{ $groupName }}
  finalizers:
    - resources-finalizer.argocd.argoproj.io
spec:
  project: {{ $appConfig.project | default $generatedName }}
  source:
    repoURL: {{ $appConfig.source.repoURL | required "source.repoURL not defined" }}
    path: {{ $path }}
    targetRevision: {{ $appConfig.source.targetRevision | default "HEAD" }}
{{- if $appConfig.isHelm }}
  {{- $defaultHelmConfig | nindent 4 }}
{{- end }}
{{- if $appConfig.source.extraConfig }}
  {{- $appConfig.source.extraConfig | nindent 4 }}
{{- end }}
  destination:
    name: {{ ternary "in-cluster" ($appConfig.destination).name ($appConfig.isRootApp | default false) | required "destinration.name not defined" }}
    namespace: {{ ternary $appConfig.argoNamespace ((($appConfig).destination).namespace | default $generatedName) ($appConfig.isRootApp | default false ) }}
{{- /* Syncpolicy will probably need to be templated too. For now, this is fine. */}}
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
      - CreateNamespace=true
{{- end }}
{{- end }}
{{- end }}
{{- end }}
